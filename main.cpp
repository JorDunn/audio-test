/*
    program: scap
    author: Jordan Dunn <jordan@nodetwo.net>
    copyright (c) 2012-2019
*/

#include <iostream>
#include <string.h>

#include "scap.h"

#define VERSION "0.0.5alpha"

void argParser(int argc, char **argv);
void help();

int main(int argc, char *argv[]) {
    argParser(argc, argv);

    return 0;
}

void argParser(int argc, char *argv[]) {
    if (argc > 1) {
        if (!strcmp(argv[1], "-f") && (argc == 3)) {
            Scap player(argv[2]);
            player.play();
        }
        else {
            help();
        }
    }
    else {
        help();
    }
}

void help() {
    std::cout << "Small Command-line Audio Player " << VERSION << std::endl;
    std::cout << "Usage is: scap -f <file>" << std::endl;
}