
#include <iostream>
#include <string>
#include <exception>

#include <SFML/System.hpp>
#include <SFML/Audio.hpp>

#include "scap.h"


Scap::Scap(std::string file) {
    this->file = file;
}

void Scap::play() {
    sf::Music player;
    try {
        player.openFromFile(this->file);
        displaySongInfo(this->file);
    }
    catch (std::exception e) {
        std::cout << "ERROR: " << e.what() << std::endl;
        exit(1);
    }
}

void Scap::displaySongInfo(std::string song) {
    // I could just cout this in the other function, but I want to expand on this later
    // with actual song info pulled from the metadata.
    std::cout << "Now playing " << song << std::endl;
}