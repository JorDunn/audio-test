# SCAP

## Usage
Its pretty simple to use:

	./scap -f <file>

or, to see the help

	./scap -h

Requires [SFML 2.1](http://www.sfml-dev.org). Darwin(MacOS and OSX) requires the homebrew version of SFML now.

## Building
To build:

	./configure
	make
	make install

This little program should compile just fine on MacOS/OSX and Linux with SFML 2.1+ (Windows is untested). Enjoy!

## Notes
* This will not compile on _ANY_ system with a SFML/CSFML below 2.1!
* [SFML supported audio formats](https://www.sfml-dev.org/faq.php#audio-formats)
