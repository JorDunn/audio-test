#ifndef _H_SCAP_
#define _H_SCAP_

class Scap {
    public:
        std::string file;

        Scap(std::string file);

        void play();

        void displaySongInfo(std::string song);
};

#endif